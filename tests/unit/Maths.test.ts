import * as Maths from '../../src/common/Maths';

describe('Maths', () => {
  describe('quadratic solver', () => {
    it('successfully solves a basic quadratic equation', () => {
      const question = 'x^2 + x = 20';
      const expectedResult = [-5, 4];
      const result = Maths.quadraticSolver(question);
      expect(result).toEqual(expect.arrayContaining(expectedResult));
    });
  });

  describe('basic algebra solver', () => {
    it('simplifies a simple expression with just x\'s', () => {
      const question = '2x+5x-7x+4x';
      const expectedResult = ['4x'];
      const result = Maths.basicAlgebraSolver(question);
      expect(result).toEqual(expectedResult);
    });

    it('simplifies an expression with multiplication in it', () => {
      const question = 'x*5x+6x/2x';
      const expectedResult = ['5x^2', '+', '3'];
      const result = Maths.basicAlgebraSolver(question);
      expect(result).toEqual(expectedResult);
    });
  });

  it('simplifies an expression with brackets in it', () => {
    const question = '4x*(3+2x*5)+(7x - 12x^2)';
    const expectedResult = ['19x', '+', '28x^2'];
    const result = Maths.basicAlgebraSolver(question);
    expect(result).toEqual(expectedResult);
  });

  it('simplifies an equation', () => {
    const question = '11x+7-3x=6+7x';
    const expectedResult = ['x', '+', '1'];
  });

});
