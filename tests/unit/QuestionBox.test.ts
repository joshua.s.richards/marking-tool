import {mount, shallowMount} from '@vue/test-utils';
import questionBox from '../../src/components/QuestionBox.vue';

describe('questionBox', () => {
  const question = 'x + 5 = 7';
  const title = "Simplify";
  it('renders the props', () => {
    const wrapper = mount(questionBox, {
      propsData: {question,title},
    });
    return wrapper.vm.$nextTick(() => {
      expect(wrapper.find('#QuestionBox__question').text()).toEqual(question);
      expect(wrapper.find('#QuestionBox__title').text()).toEqual(title);
    });
  });
});
