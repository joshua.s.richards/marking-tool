import {DynamicObject, QuadraticObject} from '@/common/interfaces/Objects';
import * as Manipulate from './VariableManipulation';

export function quadraticSolver(question: string): number[] {
  const simplification = basicAlgebraSolver(question);
  const xValueObject: QuadraticObject = {
    'x^2': '0',
    'x': '0',
    'numeric': '0',
  };
  for (let i = 0; i < simplification.length; i += 2) {
    const xValue = simplification[i].includes('x') ?
      simplification[i].slice(simplification[i].indexOf('x')) : 'numeric';
    const numericValue = simplification[i].includes('x') ? simplification[i].slice(0,
      simplification[i].indexOf('x')) || '1' : simplification[i];
    xValueObject[xValue] = i > 0 && simplification[i - 1] === '-' ? numericValue[0] === '-' ?
      numericValue : '-'.concat(
        numericValue) : numericValue;
  }
  const a = Number(xValueObject['x^2']);
  const b = Number(xValueObject.x);
  const c = Number(xValueObject.numeric);
  return [(-b + Math.sqrt(b * b - 4 * a * c)) / 2 * a, (-b - Math.sqrt(b * b - 4 * a * c)) / 2 * a];
}

export function basicAlgebraSolver(question: string): string[] {
  const arithmetic = ['*', '/', '÷', '+', '-'];
  const brackets = ['(', ')', '{', '}', '[', ']'];
  let tokens: string[] = Manipulate.splitString(question);
  tokens = Manipulate.changeSubtractToAdd(tokens);
  tokens = Manipulate.removeBrackets(tokens, arithmetic, brackets);
  const equalsIndex: number = tokens.indexOf('=');
  let leftHandSide: string[] = equalsIndex === -1 ? tokens : tokens.slice(0, equalsIndex);
  leftHandSide = sideSolver(leftHandSide);
  let rightHandSide: string[] | null = equalsIndex === -1 ? null : tokens.slice(equalsIndex + 1);
  if (rightHandSide !== null) {
    rightHandSide = sideSolver(rightHandSide);
    for (let i = 0; i < rightHandSide.length; i++) {
      if (i === 0) {
        rightHandSide[i][0] === '-' ? rightHandSide[i] = rightHandSide[i].substring(1) : rightHandSide[i] = '-'.concat(
          rightHandSide[i]);
      } else if (rightHandSide[i] === '+' || rightHandSide[i] === '-') {
        rightHandSide[i] === '+' ? rightHandSide[i] = '-' : rightHandSide[i] = '+';
      }
    }
  }
  const finalResult = rightHandSide === null ? leftHandSide : leftHandSide.concat(['+'].concat(rightHandSide));
  return additiveOperation(finalResult);
}

export function sideSolver(tokens: string[]): string[] {
  const multiplicativeOperations: string[] = ['*', '/', '÷'];
  for (const operation of multiplicativeOperations) {
    tokens = multiplicativeOperation(tokens, operation);
  }
  tokens = additiveOperation(tokens);
  return tokens;
}

export function multiplicativeOperation(tokens: string[], operation: string): string[] {
  while (tokens.some((token) => token === operation)) {
    const index = tokens.indexOf(operation);
    const leftHandSide = tokens[index - 1];
    const rightHandSide = tokens[index + 1];
    if (leftHandSide.includes('x') && rightHandSide.includes('x')) {
      const numericValue = (new Function('return ' + (leftHandSide.slice(0,
        leftHandSide.indexOf('x')) || '1') + operation + (rightHandSide.slice(0,
        rightHandSide.indexOf('x')) || '1')))();
      const leftHandSideXValue: string = leftHandSide.indexOf('^') === -1 ? '1' : leftHandSide.slice(
        leftHandSide.indexOf('^') + 1);
      const rightHandSideXValue: string = rightHandSide.indexOf('^') === -1 ? '1' : rightHandSide.slice(
        rightHandSide.indexOf('^') + 1);
      const xValue = operation === '*'
        ? (new Function('return ' + (leftHandSideXValue + '+' + rightHandSideXValue)))()
        : (new Function('return ' + (leftHandSideXValue + '-' + rightHandSideXValue)))();
      if (xValue === 0) {
        tokens.splice(index - 1, 3, `${numericValue}`);
      } else {
        tokens.splice(index - 1,
          3,
          `${numericValue}x^${xValue}`.replace(/(\^[1]+)+/g, ''));
      }
    } else if (leftHandSide.includes('x') || rightHandSide.includes('x')) {
      const leftOrRight = leftHandSide.includes('x');
      const numericValue = leftOrRight ? (new Function('return ' + ((leftHandSide.slice(0,
        leftHandSide.indexOf('x'))
        .replace('^', '') || '1') + operation + rightHandSide)))() : (new Function('return ' + (
        (leftHandSide + operation + rightHandSide.slice(0, rightHandSide.indexOf('x')).replace(
          '^',
          '') || '1'))))();
      const finalValue = `${numericValue}x^${leftOrRight ? (leftHandSide.slice(leftHandSide.indexOf(
        'x') + 1)
        .replace(
          '^',
          '') || '1') : (rightHandSide.slice(rightHandSide.indexOf('x') + 1).replace('^',
        '') || '1')}`.replace(/(\^[0-1]+)+/g, ''); // TODO separate this to make negative x values possible
      tokens.splice(index - 1,
        3, finalValue,
      );
    } else {
      const finalValue: string =
        (new Function('return ' + 'String(' + (leftHandSide + operation + rightHandSide + ')')))();
      tokens.splice(index - 1,
        3,
        finalValue);
    }
  }
  return tokens;
}

export function additiveOperation(tokens: string[]) {
  const tokenObject: DynamicObject = {};
  let tokensCopy = tokens.slice(0);
  for (let i = 0; i < tokensCopy.length; i += 2) {
    if (tokensCopy[i].includes('x')) {
      const xValue = tokensCopy[i].slice(tokensCopy[i].indexOf('x'));
      const numericValue = tokensCopy[i].slice(0, tokensCopy[i].indexOf('x')) || '1';
      if (tokenObject[xValue]) {
        tokenObject[xValue] = [...tokenObject[xValue], '+', tokensCopy[i - 1] === '-' ?
          numericValue[0] === '-' ? numericValue.substring(
            1) : `-${numericValue}` : numericValue];
      } else {
        tokenObject[xValue] = i - 1 === -1 && tokensCopy[i - 1] === '-' ? [`-${numericValue}`] :
          [numericValue];
      }
    } else {
      if (tokenObject.numeric) {
        tokenObject.numeric = [...tokenObject.numeric, '+', tokensCopy[i - 1] === '-' ?
          tokensCopy[i][0] === '-' ? tokensCopy[i].substring(
            1) : `-${tokensCopy[i]}` : tokensCopy[i]];
      } else {
        tokenObject.numeric = i - 1 === -1 && tokensCopy[i - 1] === '-' ? [`-${tokensCopy[i]}`]
          : [tokensCopy[i]];
      }
    }
    i === 0 ? tokensCopy = tokensCopy.slice(1) : tokensCopy.splice(i - 1, 2);
    i = -1;
  }
  let finalResult: string[] = [];
  for (const key of Object.keys(tokenObject)) {
    finalResult = finalResult.length > 0 ? [...finalResult, '+', `${performOperation(
      tokenObject[key])}${key === 'numeric' ? '' : key}`]
      : [`${performOperation(tokenObject[key])}${key === 'numeric' ? '' : key}`];
  }
  return finalResult;
}

export function performOperation(tokens: string[]) {
  while (tokens.some((token) => ['-', '+'].includes(token))) {
    const index = tokens.indexOf('-') === -1 ? tokens.indexOf('+') : tokens.indexOf('-');
    tokens.splice(index - 1, 3,
      (new Function(`return ${tokens[index - 1]}${tokens[index]}${tokens[index + 1]}`))());
  }
  return tokens;
}
