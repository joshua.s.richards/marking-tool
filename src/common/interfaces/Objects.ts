export interface DynamicObject {
  [key: string]: string[];
}

export interface QuadraticObject {
  [key: string]: string;
}

export interface BracketsObject {
  leftBracket: string;
  index: number;
  multiplicationFactor: string | undefined;
  symbol: string | undefined;
}
