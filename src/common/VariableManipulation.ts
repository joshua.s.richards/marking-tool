import {BracketsObject, QuadraticObject} from '@/common/interfaces/Objects';

export function changeSubtractToAdd(tokens: string[]) {
  for (let i = 0; i < tokens.length; i++) {
    if (tokens[i] === '-' && !(['(', '{', '['].includes(tokens[i + 1].replace(/([^({[])+/g,
      '')))) {
      tokens.splice(i, 2, '+', '-'.concat(tokens[i + 1]));
    }
  }
  return tokens;
}

export function removeBrackets(tokens: string[], arithmetic: string[], brackets: string[]): string[] {
  const equivalentBracket: QuadraticObject = {
    '(': ')',
    '[': ']',
    '{': '}',
  };
  const bracketsStack: BracketsObject[] = [];
  for (let i = 0; i < tokens.length; i++) {
    if (['(', '{', '['].includes(tokens[i][tokens[i].length - 1])) {
      ['*', '/', '÷'].includes(tokens[i - 1]) ? bracketsStack.unshift({
          leftBracket: tokens[i],
          index: i,
          multiplicationFactor: brackets.includes(tokens[i - 2]) ? getResultWithinBrackets(
            tokens,
            arithmetic,
            i - 2,
            tokens[i - 2],
            brackets)[0] : tokens[i - 2],
          symbol: tokens[i - 1], // TODO deal with more than one variable in bracket multiplication line above
        }) :
        bracketsStack.unshift({
          leftBracket: tokens[i],
          index: i,
          multiplicationFactor: undefined,
          symbol: undefined,
        });
    }
    if ([')', '}', ']'].includes(tokens[i][0])) {
      if (equivalentBracket[bracketsStack[0].leftBracket] === tokens[i]) {
        if (bracketsStack[0].multiplicationFactor) {
          let expression = tokens.slice(bracketsStack[0].index + 1, i);
          let length = 0;
          if ([')', '}', ']'].includes(tokens[bracketsStack[0].index - 2])) {
            length = getExpressionWithinBrackets(tokens,
              bracketsStack[0].index - 2,
              tokens[bracketsStack[0].index - 2]).length;
          }
          expression = expression.map((element, index) => !(arithmetic.includes(element)) ?
            index - 1 > 0 && ['*', '/', '÷'].includes(expression[index - 1]) ? element :
              [bracketsStack[0].multiplicationFactor, bracketsStack[0].symbol, element] : element)
            .flat();
          tokens.splice(bracketsStack[0].index - 2 - length,
            i - bracketsStack[0].index + 3 + length,
            ...expression);
        } else if (['*', '/', '÷'].includes(tokens[i + 1])) {
          let expression = tokens.slice(bracketsStack[0].index + 1, i);
          let length = 0;
          if (['(', '{', '['].includes(tokens[i + 2])) {
            length = getExpressionWithinBrackets(tokens, i + 2, tokens[i + 2]).length;
          }
          const multiplicationFactor = brackets.includes(
            tokens[i + 2]) ? getResultWithinBrackets(
            tokens,
            arithmetic,
            i + 2,
            tokens[i + 2],
            brackets)[0] : tokens[i + 2];
          expression = expression.map((element, index) => !(arithmetic.includes(element)) ?
            index + 1 < expression.length && ['*', '/', '÷'].includes(
            expression[index + 1]) ? element : [element, tokens[i + 1], multiplicationFactor] : element)
            .flat();
          tokens.splice(bracketsStack[0].index,
            i - bracketsStack[0].index + 3 + length,
            ...expression);
        } else {
          tokens.splice(bracketsStack[0].index,
            i - bracketsStack[0].index + 1,
            ...tokens.slice(bracketsStack[0].index + 1, i));
        }
        i = -1;
      }
    }
  }
  return tokens;
}

export function getResultWithinBrackets(tokens: string[], arithmetic: string[], index: number, bracket: string,
                                        brackets: string[]): string[] {
  return removeBrackets(getExpressionWithinBrackets(tokens, index, bracket),
    arithmetic,
    brackets);
}

export function getExpressionWithinBrackets(tokens: string[], index: number, bracket: string): string[] {
  const equivalentBracket: QuadraticObject = {
    '(': ')',
    '[': ']',
    '{': '}',
    ')': '(',
    ']': '[',
    '}': '{',
  };
  if (['(', '{', '['].includes(bracket)) {
    for (let i = index; i < tokens.length; i++) {
      if (equivalentBracket[bracket] === tokens[i][0]) {
        return tokens.slice(index, i + 1);
      }
    }
  } else {
    for (let i = index; i > -1; i--) {
      if (equivalentBracket[bracket] === tokens[i][tokens[i].length - 1]) {
        return tokens.slice(i, index + 1);
      }
    }
  }
  throw new Error('no brackets within expression');
}


export function splitString(question: string): string[] {
  return (question
    .split(/([\*/+\-÷=])+/g) as string[])
    .flatMap((q) => q.replace(/ /g, '').split(/([({[)}\]])/g))
    .filter((q) => q !== '' && q !== ' ');
}
